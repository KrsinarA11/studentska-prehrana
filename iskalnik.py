__author__ = 'uporabnik'

from tkinter import *
import re
from datetime import date
import prenos_podatkov

try:
    from PIL import Image, ImageTk
except:
    import webbrowser
    def link_callback(event):
        webbrowser.open_new(r"http://wp.stolaf.edu/it/installing-pil-pillow-cimage-on-windows-and-mac/")
        quit()
    okno = Tk()
    Label(okno, text='Če želite uporabiti iskalnik, si morate naložiti dodatno knjižnico PIL za prikaz slik.',
          font='Tahoma 11', bg='powder blue').grid(column=0, row=0)
    lbl_link = Label(okno, text="Knjižnico lahko najdete tukaj.", fg="Blue", cursor="hand2", font='Tahoma 11',
                     bg='powder blue')
    lbl_link.grid(column=0, row=1, sticky=EW)
    lbl_link.bind("<Button-1>", link_callback)
    okno.mainloop()
    quit()

class Iskalnik():

    def __init__(self, root):
        root.title("Študentska prehrana")
        root.config(bg='powder blue')

        self.frame = Frame(root)
        self.frame.grid(column=0, row=0)
        self.frame.config(bg='powder blue')
        Label(self.frame, text='Prenesi podatke:', font='Tahoma 15 bold', bg='powder blue')\
            .grid(column=0, row=0, columnspan=2)
        Button(self.frame, text='Hitro', font='Tahoma 11 bold', bd=3, activebackground='goldenrod1',
               command=self.nalozi_threading).grid(column=0, row=1, pady=20)
        Button(self.frame, text='Počasi', font='Tahoma 11 bold', bd=3, activebackground='goldenrod1',
               command=self.nalozi_pocasi).grid(column=2, row=1)
        Label(self.frame, text='* hitro = cca. 3min\n počasi = cca. 1h',
              font='Tahoma 11', bg='powder blue').grid(column=0, row=2, columnspan=3, sticky=W)
        Label(self.frame, text='** pri hitrem prenosu se zna zgoditi, '
                               'da bo oddaljeni gostitelj prisilno zaprl povezavo',
              font='Tahoma 11', bg='powder blue').grid(column=0, row=4, columnspan=3)

        # slika0
        global photo0
        image0 = Image.open('Smiley-Popcorn-Image.jpg')
        width_org, height_org = image0.size
        factor0 = 0.2
        width = int(width_org * factor0)
        height = int(height_org * factor0)
        image00 = image0.resize((width, height), Image.ANTIALIAS)
        photo0 = ImageTk.PhotoImage(image00)
        label0 = Label(self.frame, image=photo0)
        label0.image0 = photo0
        label0.grid(column=1, row=1, rowspan=2)

        for i in range(3):
            root.columnconfigure(i, weight=3)
        for j in range(5):
            root.rowconfigure(j, weight=3)


    def nalozi_threading(self):
        danes = str(date.today())
        prenos_podatkov.nalozi_podatke_threading(danes)
        with open('lokali-{0}.txt'.format(danes), 'r') as f:
            self.lokali = eval(f.read().strip())
        with open('vsi_kraji-{0}.txt'.format(danes), 'r') as g:
            self.vsi_kraji = eval(g.read().strip())
        self.frame.destroy()
        self.okno_iskalnik()

    def nalozi_pocasi(self):
        danes = str(date.today())
        prenos_podatkov.nalozi_podatke_pocasi(danes)
        with open('lokali-{0}.txt'.format(danes), 'r') as f:
            self.lokali = eval(f.read().strip())
        with open('vsi_kraji-{0}.txt'.format(danes), 'r') as g:
            self.vsi_kraji = eval(g.read().strip())
        self.frame.destroy()
        self.okno_iskalnik()

    def okno_iskalnik(self):
        # slika1
        image1 = Image.open('chef-garfield.gif')
        width_org, height_org = image1.size
        factor1 = 0.8
        width = int(width_org * factor1)
        height = int(height_org * factor1)
        image11 = image1.resize((width, height), Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(image11)
        label = Label(root, image=photo)
        label.image1 = photo
        label.grid(column=0, row=0, rowspan=4, columnspan=3)

        # slika2
        gif = Image.open('drooling-smiley.png')
        width_org, height_org = gif.size
        factor = 0.18
        width = int(width_org * factor)
        height = int(height_org * factor)
        gif1 = gif.resize((width, height), Image.ANTIALIAS)
        photo = ImageTk.PhotoImage(gif1)
        label = Label(root, image=photo)
        label.gif1 = photo
        label.grid(column=4, row=7, rowspan=3, columnspan=2)

        Label(root, bg='powder blue', text='Bon appetit !!',
              font='Script 40 bold italic').grid(column=4, row=1)

        # vnos jedi
        Label(root, bg='white', text='Jed:', font='Tahoma 12 bold').grid(column=0, row=2)
        self.jed = StringVar()
        jed = Entry(root, textvariable=self.jed, bd=3)
        jed.grid(column=1, row=2, columnspan=5)

        # izbira kraja
        kraji = list(self.vsi_kraji)
        kraji.sort()
        kraji = ['vsi'] + kraji
        Label(root, bg='white', text='Kraj:', font='Tahoma 12 bold').grid(column=0, row=3)
        self.kraj = StringVar(root)
        self.kraj.set('vsi')
        self.menu_kraj = OptionMenu(root, self.kraj, *kraji)
        self.menu_kraj.config(font='Tahoma 11 bold', activebackground='goldenrod1')
        self.menu_kraj.grid(column=1, row=3, columnspan=5)

        # vnos imena
        Label(root, bg='white', text='Ime:', font='Tahoma 12 bold').grid(column=0, row=4)
        self.ime = StringVar()
        Entry(root, textvariable=self.ime, bd=3).grid(column=1, row=4, columnspan=5)

        # vnos ulice
        Label(root, bg='white', text='Ulica:', font='Tahoma 12 bold').grid(column=0, row=5)
        self.ulica = StringVar()
        Entry(root, textvariable=self.ulica, bd=3).grid(column=1, row=5, columnspan=5)

        # vnos doplačila
        Label(root, bg='white', text='Doplačilo v €:', font='Tahoma 12 bold').grid(column=0, row=6)
        Label(root, bg='powder blue', text='od:', font='Tahoma').grid(column=1, row=6, sticky=E)
        Label(root, bg='powder blue', text='do:', font='Tahoma').grid(column=3, row=6, sticky=E)
        self.min_dopl = StringVar()
        Entry(root, textvariable=self.min_dopl, width=5, bd=3).grid(column=2, row=6, sticky=W)
        self.max_dopl = StringVar()
        Entry(root, textvariable=self.max_dopl, width=5, bd=3).grid(column=4, row=6, sticky=W)

        # izbira ali je lokal odprt v soboto in nedeljo
        Label(root, bg='white', text='Odprto tudi:', font='Tahoma 12 bold').grid(column=0, row=7)
        self.sobota = IntVar()
        Checkbutton(root, bg='powder blue', text='sobota', font='Tahoma', variable=self.sobota)\
            .grid(column=2, row=7, columnspan=2)
        self.nedelja = IntVar()
        Checkbutton(root, bg='powder blue', text='nedelja', font='Tahoma', variable=self.nedelja)\
            .grid(column=2, row=8, columnspan=2)

        # gumb za iskanje
        Button(root, bg='white', bd=7, text='IŠČI', font='Tahoma 15 bold', activebackground='goldenrod1',
               command=self.isci).grid(column=0, row=9, columnspan=6)

        # listbox in scrollbar z zadetki iskanja
        self.zadetki = Listbox(root, width=42, height=26, bd=7, font='Tahoma 12', bg='LightGoldenrod1')
        y_scrollbar = Scrollbar(root)
        y_scrollbar.grid(column=7, row=0, rowspan=10, sticky=N+S)
        self.zadetki.config(yscrollcommand=y_scrollbar.set)
        y_scrollbar.config(command=self.zadetki.yview)
        x_scrollbar = Scrollbar(root, orient=HORIZONTAL)
        x_scrollbar.grid(column=6, row=10, sticky=W+E)
        self.zadetki.config(xscrollcommand=x_scrollbar.set)
        x_scrollbar.config(command=self.zadetki.xview)
        self.zadetki.grid(column=6, row=0, rowspan=10)
        self.zadetki.bind("<Double-Button-1>", self.izpis_podatkov)

        # izpis podatkov izbranega lokala
        self.izpis = Text(root, height=27, width=42, bd=11, font='Tahoma 12', bg='LightGoldenrod1', wrap=NONE)
        y_scrollbar = Scrollbar(root)
        y_scrollbar.grid(column=9, row=0, rowspan=10, sticky=N+S)
        self.izpis.config(yscrollcommand=y_scrollbar.set)
        y_scrollbar.config(command=self.izpis.yview)
        x_scrollbar = Scrollbar(root, orient=HORIZONTAL)
        x_scrollbar.grid(column=8, row=10, sticky=W+E)
        self.izpis.config(xscrollcommand=x_scrollbar.set)
        x_scrollbar.config(command=self.izpis.xview)
        self.izpis.grid(column=8, row=0, rowspan=10)

        # prilagodi velikost vrstic in stolpcev, če spremenimo velikost okna
        for i in range(9):
            root.columnconfigure(i, weight=3)
        for j in range(11):
            root.rowconfigure(j, weight=3)

        root.bind('<Return>',self.isci)
        jed.focus()

    # funkcija za iskanje
    def isci(self, *arg):

        # nastavi ime
        par_ime = self.ime.get()

        # nastavi kraj
        par_kraj = self.kraj.get()

        # nastavi ulico
        par_ulica = self.ulica.get()

        # preveri ceno
        par_min = re.sub(r',', r'.', self.min_dopl.get())
        par_max = re.sub(r',', r'.', self.max_dopl.get())

        # nastavi jed
        par_jed = self.jed.get().lower()

        # preveri, če je vpisana cena res številka
        opozorilo = ''
        if par_min != '':
            try:
                par_min = float(par_min)
            except:
                opozorilo = '{0} ni število!!!'.format(par_min)
        if par_max != '':
            try:
                par_max = float(par_max)
            except:
                opozorilo = '{0} ni število!!!'.format(par_max)

        if opozorilo == '':
            ujemanje = []
            for lokal, p in self.lokali.items():
                if par_ime != '':
                    if not re.match('[^=]*?'+par_ime+'[^=]*?', lokal, re.IGNORECASE):
                        continue
                if par_kraj != 'vsi':
                    if par_kraj != p['kraj']:
                        continue
                if par_ulica != '':
                    if not re.match('[^=]*?'+par_ulica+'[^=]*?', p['naslov'], re.IGNORECASE):
                        continue
                if par_min != '':
                    if p['doplacilo'] < float(par_min):
                        continue
                if par_max != '':
                    if p['doplacilo'] > float(par_max):
                        continue
                if self.sobota.get() == 1:
                    if p['sobota'] == 'Lokal ob sobotah ne nudi subvencionirane študentske prehrane.':
                        continue
                if self.nedelja.get() == 1:
                    if p['nedelja'] == 'Lokal ob nedeljah ne nudi subvencionirane študentske prehrane.':
                        continue
                if par_jed != '':
                    jed = False
                    for meni in p['jedilnik']:
                        for e in meni:
                            if re.match("[^']*?"+par_jed+"[^']*?", e, re.IGNORECASE):
                                jed = True
                    if not jed:
                        continue
                ujemanje += [lokal]
            ujemanje.sort()
            self.zadetki.delete(0, END)
            for l in ujemanje:
                self.zadetki.insert(END, l)
        else:
            self.izpis.delete('1.0', END)
            self.izpis.insert(END, 'Ne delaj se norca in vpiši veljavno število!!!')

    # funkcija za izpis podatkov o izbranem lokalu
    def izpis_podatkov(self, call):
        izbira = self.zadetki.get(ACTIVE)
        p = self.lokali[izbira]
        if isinstance(p['jedilnik'], str):
            jedilnik = p['jedilnik']
        else:
            jedilnik = ''
            for i, meni in enumerate(p['jedilnik']):
                jedilnik += '\n'
                jedilnik += '  {0}. meni:\n'.format(i+1)
                for jed in meni:
                    jedilnik += ' '*10+jed+'\n'
        text = 'ime: {0}\n\nnaslov: {1}\n\nkraj: {2}\n\ndoplačilo: {3} €\n\ndelovni čas:\n   delavniki: {4}\n   ' \
               'sobote: {5}\n   nedelje: {6}\n\njedilnik: {7}'.format(izbira, p['naslov'], p['kraj'], p['doplacilo'],
                                                                      p['delavnik'], p['sobota'], p['nedelja'], jedilnik)

        self.izpis.delete('1.0', END)
        self.izpis.insert(END, text)


root = Tk()
app = Iskalnik(root)
root.mainloop()
