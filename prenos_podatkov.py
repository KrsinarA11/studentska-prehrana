__author__ = 'uporabnik'

import urllib.request
import re
import time
import threading
import os
import random
import datetime


# funkcija pogleda, če že imamo podatke, če jih nimamo, kliče funkcijo, ki jih prebere s spleta
def nalozi_podatke_threading(danes):
    prej = time.time()
    datoteke = os.listdir()
    datum = ''
    for dat in datoteke:
        if dat[:6] == 'lokali':
            datum = dat[-14:-4]
    if datum < danes:
        lokali = prenesi_podatke_threading()
        # množica vseh krajev
        vsi_kraji = set()
        for l in lokali.values():
            vsi_kraji = vsi_kraji | {l['kraj']}
        if datum:
            os.rename('lokali-{0}.txt'.format(datum), 'lokali-{0}.txt'.format(danes))
            os.rename('vsi_kraji-{0}.txt'.format(datum), 'vsi_kraji-{0}.txt'.format(danes))
        with open('lokali-{0}.txt'.format(danes), 'w') as f:
            f.write(str(lokali))
        with open('vsi_kraji-{0}.txt'.format(danes), 'w') as f:
            f.write(str(vsi_kraji))
        potem = time.time()
        print(datetime.timedelta(seconds=potem - prej))


def nalozi_podatke_pocasi(danes):
    prej = time.time()
    datoteke = os.listdir()
    datum = ''
    for dat in datoteke:
        if dat[:6] == 'lokali':
            datum = dat[-14:-4]
    if datum < danes:
        lokali = prenesi_podatke_pocasi()
        # množica vseh krajev
        vsi_kraji = set()
        for l in lokali.values():
            vsi_kraji = vsi_kraji | {l['kraj']}
        if datum:
            os.rename('lokali-{0}.txt'.format(datum), 'lokali-{0}.txt'.format(danes))
            os.rename('vsi_kraji-{0}.txt'.format(datum), 'vsi_kraji-{0}.txt'.format(danes))
        with open('lokali-{0}.txt'.format(danes), 'w') as f:
            f.write(str(lokali))
        with open('vsi_kraji-{0}.txt'.format(danes), 'w') as f:
            f.write(str(vsi_kraji))
        potem = time.time()
        print(datetime.timedelta(seconds=potem - prej))


# funkcija, ki ugotovi, če je lokal odprt - zaprtih lokalov ne dodamo v slovar
def odprt(podatek):
    m = re.findall(r'blocked">[^\(]*?(\()[^\(]*?</span>', podatek[0])
    if m:
        return False
    else:
        return True


# funkcija, ki doda podatke o enem lokalu v slovar
def dodaj_lokal(st, podatki, e_naslovi):
    l = podatki[st]
    if odprt(l):
        v = re.sub(r"(amp;)", "", e_naslovi[st])  # popravi naslov spletne strani
        k = l[2].split(',')
        if len(k) == 3:
            n_ = k[0]+','+k[1]
            m_ = k[2]
            k = [n_, m_]
        dp = l[3].replace(',', '.')
        # odpre stran z jedilnikom in stran s podatki o delovnem času
        lokal_jedi = urllib.request.urlopen(v).read().decode('utf-8')
        s = random.randint(3, 7)
        time.sleep(s)
        lokal = urllib.request.urlopen(v[:-1]+'1').read().decode('utf-8')
        d = re.findall(r'(od \d+:\d+ do \d+:\d+)', lokal)
        s, n = 'Lokal ob sobotah ne nudi subvencionirane študentske prehrane.', \
               'Lokal ob nedeljah ne nudi subvencionirane študentske prehrane.'
        if len(d) > 1:
            s = d[1]
        if len(d) > 2:
            n = d[2]
        d = d[0]
        jedi = re.findall(r'<ul  class=.+? title=.+?>\s*?(.*?)\s*?<div class="imgtwo"', lokal_jedi, re.DOTALL)
        if jedi:
            jedilnik = []
            for j in jedi:
                jedilnik += [re.findall(r'<li>\s*?(.*?)\s*?</li>', j.lower())]
        else:
            jedilnik = 'Za izbran datum ni podatkov o jedilniku.'
        return {'naslov': k[0], 'kraj': k[1].strip(), 'doplacilo': float(dp), 'delavnik': d, 'sobota': s, 'nedelja': n,
                'jedilnik': jedilnik}


# funkcija za en thread
def en_thread(ids, slovar, podatki, e_naslovi):
    for ID in ids:
        d = dodaj_lokal(ID, podatki, e_naslovi)
        if d:
            slovar[podatki[ID][1]] = d


# funkcija iz spleta prebere podatke
def prenesi_podatke_threading():
    # prebere imenik lokalov s spleta in poišče imena lokalov, njihove naslove, ceno doplačila, naslove spletnih strani
    imenik_lokalov_html = urllib.request.urlopen('https://www.studentska-prehrana.si/Pages/Directory.aspx')\
        .read().decode('utf-8')
    podatki = re.findall(r'(class="name".*?https://[^"]+">(.*?)</a>\s*?</h1>\s*?<h2>\(([^)]*)\)</h2>.*?'
                         r'Vrednost doplačila:</span>\s*?<strong>(\d+,\d+) EUR</strong>)', imenik_lokalov_html,
                         re.DOTALL)
    e_naslovi = re.findall(r'"name">\s*?<h1>\s*?<a id="[^"]*" href="(https://[^"]+)"', imenik_lokalov_html)
    st_lokalov = len(e_naslovi)
    c = st_lokalov // 10
    o = st_lokalov % 10
    ids_list = [range(10*x, 10*x+10) for x in range(0, c)]+[range(10*c, 10*c+o)]
    threads = []
    lokali = {}
    for IDs in ids_list:
        threads.append(threading.Thread(target=en_thread, args=(IDs, lokali, podatki, e_naslovi)))
    for t in threads:
        t.start()
    for t in threads:
        t.join()
    return lokali


def prenesi_podatke_pocasi():
    # prebere imenik lokalov s spleta in poišče imena lokalov, njihove naslove, ceno doplačila, naslove spletnih strani
    imenik_lokalov_html = urllib.request.urlopen('https://www.studentska-prehrana.si/Pages/Directory.aspx')\
        .read().decode('utf-8')
    podatki = re.findall(r'(class="name".*?https://[^"]+">(.*?)</a>\s*?</h1>\s*?<h2>\(([^)]*)\)</h2>.*?'
                         r'Vrednost doplačila:</span>\s*?<strong>(\d+,\d+) EUR</strong>)', imenik_lokalov_html,
                         re.DOTALL)
    e_naslovi = re.findall(r'"name">\s*?<h1>\s*?<a id="[^"]*" href="(https://[^"]+)"', imenik_lokalov_html)
    lokali = {}
    st_lokalov = len(e_naslovi)
    for ID in range(st_lokalov):
        d = dodaj_lokal(ID, podatki, e_naslovi)
        if d:
            lokali[podatki[ID][1]] = d
        s = random.randint(3, 7)
        time.sleep(s)
    return lokali

