# README #

### Čemu je ta repozitorij namenjen? ###

*Iskanju po študentski prehrani (išče se lahko po jedeh, krajih, restavracijah, doplačilu, ...).
*Podatki so iz spletne strani https://www.studentska-prehrana.si.


### Kako zagnati program? ###

* Z datoteko iskalnik.py (vse ostalo potrebno program naredi namesto vas, morda vas bo le opozoril, da si morate naložiti dodatno knjižnico PIL za prikaz slik)


### Na koga se obrniti za več informacij? ###

* Tatiana Sušnik
* Talita Rosec